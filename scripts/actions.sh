#!/bin/bash

Download() {

	if [[ -n "$1" && -n "$2" ]]; then

		if command -v curl >/dev/null 2>&1; then

			curl \
				--fail \
				--http2-prior-knowledge \
				--location \
				--output "$1" \
				--show-error \
				--silent \
				--ssl-reqd \
				--tlsv1.3 \
				--url "$2"
		fi
	else
		echo "Exiting...!"

		exit 1
	fi
}

Replace() {

	find .tmp -type f '(' -name "_headers" -o -name "*.html" -o -name "*.json" -o -name "*.txt" -o -name "*.webmanifest" -o -name "*.xml" ')' | sort | while read -r FILE; do

		sed -i "s%https://aeonquake.eu.org%https://staging.aeonquake.eu.org%g" "$FILE"
		sed -i "s%https://about.soracloud.eu.org%https://staging.about.soracloud.eu.org%g" "$FILE"
		sed -i "s%https://lotns.eu.org%https://staging.lotns.eu.org%g" "$FILE"
		sed -i "s%https://notryan.eu.org%https://staging.notryan.eu.org%g" "$FILE"
		sed -i "s%https://soraapis.eu.org%https://staging.soraapis.eu.org%g" "$FILE"
		sed -i "s%https://sorablog.eu.org%https://staging.sorablog.eu.org%g" "$FILE"
		sed -i "s%https://soracdns.eu.org%https://staging.soracdns.eu.org%g" "$FILE"
		sed -i "s%https://soradns.eu.org%https://staging.soradns.eu.org%g" "$FILE"
		sed -i "s%https://sorafonts.eu.org%https://staging.sorafonts.eu.org%g" "$FILE"
		sed -i "s%https://soralicense.eu.org%https://staging.soralicense.eu.org%g" "$FILE"
		sed -i "s%https://sorastatus.eu.org%https://staging.sorastatus.eu.org%g" "$FILE"
		sed -i "s%https://unordinary.eu.org%https://staging.unordinary.eu.org%g" "$FILE"
	done
}

if [[ "$CI" = "true" ]]; then

	CURRENT="$(git log -1 --format=%ct)"
	BRANCH="$(git branch --show-current)"
	LATEST="$(date +%s)"
	REPO="$(basename -s .git "$(git remote get-url origin)" | tr '[:upper:]' '[:lower:]')"
	TIME_DIFF="$((LATEST - CURRENT))"

	export CURRENT
	export BRANCH
	export LATEST
	export REPO
	export TIME_DIFF

	if [[ "$TIME_DIFF" -lt 3600 ]]; then

		sudo apt-get update >/dev/null 2>&1

		sudo apt-get upgrade --yes >/dev/null 2>&1
	fi

	if ! command -v npm >/dev/null 2>&1; then

		sudo apt-get install nodejs >/dev/null 2>&1
	fi

	if ! [[ -d "$HOME/.local/bin" ]]; then

		mkdir --parents "$HOME/.local/bin"
	fi

	if [[ -s static/bin/index-latest ]]; then

		cp static/bin/index-latest "$HOME/.local/bin/index"
	else
		if [[ "$BRANCH" = "main" ]]; then

			Download "$HOME/.local/bin/index" "https://soracdns.eu.org/bin/index-latest"
		else
			Download "$HOME/.local/bin/index" "https://staging.soracdns.eu.org/bin/index-latest"
		fi

	fi

	if [[ -d "$HOME/.local/bin" ]]; then

		export PATH="$HOME/.local/bin:$PATH"
	fi

	chmod +x "$HOME/.local/bin/index"

	if [[ -s scripts/run.sh ]]; then

		bash scripts/run.sh
	fi

	if command -v index >/dev/null 2>&1; then

		index Setup

		if [[ -d app ]]; then

			if [[ "$BRANCH" = "main" ]]; then

				index Generate --icons --production

			elif [[ "$BRANCH" != "main" || "$GITHUB_EVENT_NAME" = "workflow_dispatch" ]]; then

				index Generate --icons
			fi
		else
			index Pretty

			index ShPretty
		fi
	fi

	if [[ "$GITHUB_WORKFLOW" = "Main" || "$GITHUB_EVENT_NAME" = "workflow_dispatch" ]]; then

		if [[ -n "$(git status --short)" ]]; then

			if [[ "$GITHUB_WORKFLOW" = "Main" ]]; then

				git checkout -b main-development
			else
				git checkout -b staging-development
			fi

			git add --all

			git commit \
				--all \
				--signoff \
				--message "[Automated CI/CD] Update ${BRANCH^^} $(date)

Changes:

$(git status --short)"

			git push --all origin
		fi
	else
		git status --short
	fi

	git --no-pager diff

	if [[ "$TIME_DIFF" -lt 3600 ]]; then

		if [[ -d static ]]; then

			if [[ "$GITHUB_WORKFLOW" = "Staging" ]]; then

				tree -a "$TMPDIR/index"

				git reset --hard

				index Optimize
			fi

			if [[ -s .env ]]; then

				source .env
			fi

			if [[ "$GITHUB_WORKFLOW" = "Main" && "$BRANCH" = "main" ]]; then

				if [[ -n "$VERCEL_ORG_ID" && -n "$VERCEL_PROJECT_ID" && -n "$VERCEL_TOKEN" ]]; then

					if ! command -v vercel >/dev/null 2>&1; then

						npm install --global vercel@latest >/dev/null 2>&1
					fi

					if command -v vercel >/dev/null 2>&1; then

						vercel \
							pull \
							--cwd .tmp \
							--environment production \
							--token "$VERCEL_TOKEN" \
							--yes >"$TMPDIR/vercel.log"

						vercel \
							build \
							--cwd .tmp \
							--prod \
							--token "$VERCEL_TOKEN" >>"$TMPDIR/vercel.log"

						vercel \
							deploy \
							--cwd .tmp \
							--prebuilt \
							--prod \
							--token "$VERCEL_TOKEN" >>"$TMPDIR/vercel.log"

						grep -i ".vercel.app" "$TMPDIR/vercel.log"

						rm --force --recursive .tmp/.vercel
					fi
				fi

				if [[ -n "$CLOUDFLARE_ACCOUNT_ID" && -n "$CLOUDFLARE_API_TOKEN" ]]; then

					if ! command -v wrangler >/dev/null 2>&1; then

						npm install --global wrangler@latest >/dev/null 2>&1
					fi

					if command -v wrangler >/dev/null 2>&1; then

						wrangler \
							pages \
							project \
							create "${REPO//_/-}" \
							--production-branch main >/dev/null 2>&1

						wrangler \
							pages \
							deploy .tmp \
							--branch main \
							--commit-dirty true \
							--project-name "${REPO//_/-}"
					fi
				fi

			elif [[ "$GITHUB_WORKFLOW" = "Staging" && "$BRANCH" = "staging" ]]; then

				Replace

				if [[ -n "$NETLIFY_AUTH_TOKEN" && -n "$NETLIFY_SITE_ID" ]]; then

					if ! command -v netlify >/dev/null 2>&1; then

						npm install --global netlify-cli@latest >/dev/null 2>&1
					fi

					if command -v netlify >/dev/null 2>&1; then

						netlify \
							deploy \
							--dir ".tmp" \
							--prod >"$TMPDIR/netlify.log"

						grep -i ".netlify.app" "$TMPDIR/netlify.log"
					fi
				fi

				if [[ -n "$CLOUDFLARE_ACCOUNT_ID" && -n "$CLOUDFLARE_API_TOKEN" ]]; then

					if ! command -v wrangler >/dev/null 2>&1; then

						npm install --global wrangler@latest >/dev/null 2>&1
					fi

					if command -v wrangler >/dev/null 2>&1; then

						wrangler \
							pages \
							project \
							create "${REPO//_/-}" \
							--production-branch main >/dev/null 2>&1

						wrangler \
							pages \
							deploy .tmp \
							--branch staging \
							--commit-dirty true \
							--project-name "${REPO//_/-}"
					fi
				fi
			fi
		fi
	fi
fi
