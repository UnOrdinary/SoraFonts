#!/bin/bash

rm --force --recursive "static/css" "static/s" "$TMPDIR/run"

mkdir --parents "static/css" "static/s/bootstrap" "$TMPDIR/run"

index Download static/css/bootstrap-icons.css "https://raw.githubusercontent.com/twbs/icons/main/font/bootstrap-icons.css"
index Download static/css/bootstrap-icons.min.css "https://raw.githubusercontent.com/twbs/icons/main/font/bootstrap-icons.min.css"
index Download static/s/bootstrap/bootstrap-icons.woff2 "https://raw.githubusercontent.com/twbs/icons/main/font/fonts/bootstrap-icons.woff2"
index Download static/s/bootstrap/bootstrap-icons.woff "https://raw.githubusercontent.com/twbs/icons/main/font/fonts/bootstrap-icons.woff"
index Download static/s/bootstrap/bootstrap-icons.svg "https://raw.githubusercontent.com/twbs/icons/main/bootstrap-icons.svg"

sed -i -e 's|./fonts/bootstrap-icons.woff2|/s/bootstrap/bootstrap-icons.woff2|g' static/css/bootstrap-icons.css
sed -i -e 's|./fonts/bootstrap-icons.woff|/s/bootstrap/bootstrap-icons.woff|g' static/css/bootstrap-icons.css

sed -i -e 's|fonts/bootstrap-icons.woff2|/s/bootstrap/bootstrap-icons.woff2|g' static/css/bootstrap-icons.min.css
sed -i -e 's|fonts/bootstrap-icons.woff|/s/bootstrap/bootstrap-icons.woff|g' static/css/bootstrap-icons.min.css

while IFS='|' read -r FONTS; do

	SMALL_LETTER="${FONTS,,}"

	WITHOT_SPACE="${SMALL_LETTER// /}"

	if ! [[ -s "$TMPDIR/run/$WITHOT_SPACE.zip" ]]; then

		index Download "$TMPDIR/run/$WITHOT_SPACE.zip" "https://gwfh.mranftl.com/api/fonts/${SMALL_LETTER// /-}?download=zip&subsets=cyrillic,cyrillic-ext,greek,greek-ext,latin,latin-ext,vietnamese&variants=100,200,300,400,500,600,700,800,900,1000,100italic,200italic,300italic,regular,italic,500italic,600italic,700italic,800italic,900italic,1000italic&formats=woff2"
	fi

	unzip "$TMPDIR/run/$WITHOT_SPACE.zip" -d "$TMPDIR/run/$WITHOT_SPACE"

	tree -a "$TMPDIR/run/$WITHOT_SPACE"

	mv "$TMPDIR/run/$WITHOT_SPACE/" "static/s/"

	if ! [[ -s "static/css/$WITHOT_SPACE.css" ]]; then

		cat <<EOF >"static/css/$WITHOT_SPACE.css"
@charset "UTF-8";
/*!
 * ${WITHOT_SPACE^^}.CSS v1.0.0 (https://sorafonts.eu.org)
 * Copyright 2014-$(date +%Y) Light Of The Night Sky (https://lotns.eu.org)
 * Licensed under MIT (https://soralicense.eu.org)
 */
EOF
	fi

	find "static/s/$WITHOT_SPACE" -type f '(' -name "*.woff2" ')' | sort | while read -r FILE; do

		weight() {
			if [[ "$FILE" = *"-100italic."* || "$FILE" = *"-100."* ]]; then
				echo -n "100"

			elif [[ "$FILE" = *"-200italic."* || "$FILE" = *"-200."* ]]; then
				echo -n "200"

			elif [[ "$FILE" = *"-300italic."* || "$FILE" = *"-300."* ]]; then
				echo -n "300"

			elif [[ "$FILE" = *"-italic."* || "$FILE" = *"-regular."* ]]; then
				echo -n "400"

			elif [[ "$FILE" = *"-500italic."* || "$FILE" = *"-500."* ]]; then
				echo -n "500"

			elif [[ "$FILE" = *"-600italic."* || "$FILE" = *"-600."* ]]; then
				echo -n "600"

			elif [[ "$FILE" = *"-700italic."* || "$FILE" = *"-700."* ]]; then
				echo -n "700"

			elif [[ "$FILE" = *"-800italic."* || "$FILE" = *"-800."* ]]; then
				echo -n "800"

			elif [[ "$FILE" = *"-900italic."* || "$FILE" = *"-900."* ]]; then
				echo -n "900"

			elif [[ "$FILE" = *"-1000italic."* || "$FILE" = *"-1000."* ]]; then
				echo -n "1000"

			fi
		}

		cat <<EOF >>"static/css/$WITHOT_SPACE.css"
@font-face {
font-family: "$FONTS";
src: url("${FILE//static/}") format("woff2");
font-weight: $(weight);
font-style: $([[ "$FILE" = *"italic."* ]] && echo -n "italic" || echo -n "normal");
font-display: swap;
}
EOF

	done

done <list

if [[ -s "static/css/redhatdisplay.css" && -s "static/css/redhatmono.css" && -s "static/css/redhattext.css" ]]; then

	cat <<EOF >static/fonts.css.tmp1
@charset "UTF-8";
/*!
 * FONTS.CSS v1.0.0 (https://sorafonts.eu.org)
 * Copyright 2014-$(date +%Y) Light Of The Night Sky (https://lotns.eu.org)
 * Licensed under MIT (https://soralicense.eu.org)
 */
body *{font-family:UnOrdinary;}
article *{font-family:"UnOrdinary Text";}
code,kbd,pre,samp,pre code *{font-family:"UnOrdinary Mono";}
.index-code *{font-family:Audiowide, Comfortaa, "DM Mono", "Grape Nuts", "Hepta Slab", "Josefin Slab", Jost, "Kumbh Sans", Lexend, "Libre Barcode 128 Text", Monoton, Montserrat, "Montserrat Alternates", MuseoModerno, "Noto Sans", "Noto Sans Display", "Noto Sans Mono", "Noto Sans Symbols", "Noto Serif", "Noto Serif Display", "Open Sans", Outfit, Poppins, Quicksand, Raleway, "Readex Pro", "Red Hat Display", "Red Hat Mono", "Red Hat Text", Roboto, "Roboto Condensed", "Roboto Flex", "Roboto Mono", "Roboto Slab", "Rubik Burned", Sacramento, Satisfy, Sen, Sevillana, Sora, "Sulphur Point", Ubuntu, "Work Sans";}
EOF
	{
		grep -v "@charset \"UTF-8\";" "static/css/redhatdisplay.css"
		grep -v "@charset \"UTF-8\";" "static/css/redhatmono.css"
		grep -v "@charset \"UTF-8\";" "static/css/redhattext.css"

	} >static/fonts.css.tmp2

	sed -i -e "s/Red Hat Display/UnOrdinary/g" static/fonts.css.tmp2

	sed -i -e "s/Red Hat Mono/UnOrdinary Mono/g" static/fonts.css.tmp2

	sed -i -e "s/Red Hat Text/UnOrdinary Text/g" static/fonts.css.tmp2

	if [[ -s static/fonts.css.tmp1 && -s static/fonts.css.tmp2 ]]; then

		cat static/fonts.css.tmp1 static/fonts.css.tmp2 >static/fonts.css

		rm static/fonts.css.tmp1 static/fonts.css.tmp2
	fi
fi
